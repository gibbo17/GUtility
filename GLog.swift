//
//  GLog.swift
//  PromotionLibrary
//
//  Created by Gilberto Santaniello on 19/09/17.
//  Copyright © 2017 hutchds. All rights reserved.
//

import Foundation

public class GLog{
    
    //MARK: Variables
    var fileHandle:UnsafeMutablePointer<FILE>? = nil
    
    //Check if i want only log on console or i want log in console and write this in a file(in document directory)
    static var useOnlyLog = true
    
    //Starndard value is "DISABLED" beause i can set log but in this moment i won't see
    static let DISABLED: Int32 = 99
    
    //Set log level of file and console
    static var fileLevel = DISABLED
    static var consoleLevel = DISABLED
    
    //Name of folder
    static var nameOfFolder = "LogFolder"
    
    //tag default
    static var tagDefault = ""
    
    init() {}
    
    //SharedInstance
    static let sharedInstance : GLog = {
        let instance = GLog()
        return instance
    }()
    
    //MARK: Public method to call
    
    //Configure only log
    public static func configureOnlyLog(consoleLogLevel: Int32){
        
        //Check if use only log
        useOnlyLog = true
        
        //CONSOLE LOG
        //save level of log on console
        consoleLevel = consoleLogLevel
        
        //Set default tag with bundle id od app host
        setDefaultTag()
    }
    
    //Configure log and file (call in didFinishLaunching on Appdelegate)
    public static func configureWithFile(nameOfFile:String, createFile:Bool, consoleLogLevel: Int32, fileLogLevel:Int32){
        
        //Check if use log and file
        useOnlyLog = false
        
        //FILE LOG
        if createFile{
            let logPath_ = getDocumentDirectory().appendingPathComponent(nameOfFolder)
            
            if !getDocumentDirectory().contains(nameOfFolder){
                do {
                    try FileManager.default.createDirectory(atPath: logPath_.description, withIntermediateDirectories: false, attributes: nil)
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd_MM"
            formatter.timeZone = TimeZone.current
            let dateStr = formatter.string(from: Date())
            
            let logpath = getDocumentDirectory().appendingPathComponent("\(nameOfFolder)/\(nameOfFile)_\(dateStr).log")
            NSLog("logpathExts, \(logpath)", LOG_INFO)
            
            //Create file with data
            GLog.sharedInstance.fileHandle = fopen(logpath.cString(using: String.Encoding.ascii)!, "a+")
            
            //Save level of file log only if create file is true
            fileLevel = fileLogLevel
            
        }
        
        //CONSOLE LOG
        //Save level of log on console
        consoleLevel = consoleLogLevel
        
        //Set default tag with bundle id od app host
        setDefaultTag()
    }
    
    //Type of log message
    
    //level emergency
    public static func x(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_EMERG)
    }
    
    public static func x(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_EMERG)
    }
    
    //level alert
    public static func a(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_ALERT)
    }
    
    public static func a(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_ALERT)
    }
    
    //level critical
    public static func c(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_CRIT)
    }
    
    public static func c(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_CRIT)
    }
    
    //level error
    public static func e(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_ERR)
    }
    
    public static func e(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_ERR)
    }
    
    //level warning
    public static func w(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_WARNING)
    }
    
    public static func w(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_WARNING)
    }
    
    //level notice
    public static func n(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_NOTICE)
    }
    
    public static func n(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_NOTICE)
    }
    
    //level info
    public static func i(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_INFO)
    }
    
    public static func i(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_INFO)
    }
    
    //level debug
    public static func d(tag:String, text:String) {
        writeFileAndAddLog(tag: tag, text: text, logLevel: LOG_DEBUG)
    }
    
    public static func d(text:String) {
        writeFileAndAddLog(tag: GLog.tagDefault, text: text, logLevel: LOG_DEBUG)
    }
    
    //Get all file log
    public static func getAllFileLog() -> [String]? {
        let folder = getDocumentDirectory().appendingPathComponent(nameOfFolder)
        return try? FileManager.default.contentsOfDirectory(atPath: folder)
    }
    
    //Remove all file in directory of logs
    public static func removeAllFileLog(){
        let folder = getDocumentDirectory().appendingPathComponent(nameOfFolder)
        do {
            let filePaths = try FileManager.default.contentsOfDirectory(atPath: folder)
            for filePath in filePaths {
                try FileManager.default.removeItem(atPath: "\(folder)/\(filePath)")
            }
        } catch {
            print("Could not clear \(nameOfFolder) folder: \(error)")
        }
    }
    
    //Remove a specific file in directory of logs (don't forget append _dd_MM at name of file)
    //  **** example ***** logConsole_28_09
    public static func removeFileLog(fileName:String){
        let folder = getDocumentDirectory().appendingPathComponent(nameOfFolder)
        do {
            let filePaths = try FileManager.default.contentsOfDirectory(atPath: folder)
            for filePath in filePaths {
                if filePath == "\(fileName).log" {
                    try FileManager.default.removeItem(atPath: "\(folder)/\(filePath)")
                }
            }
        } catch {
            print("Could not clear \(nameOfFolder) folder: \(error)")
        }
    }
    
    //MARK: Internal method
    
    //Write on file and see log on console
    static func writeFileAndAddLog(tag:String, text:String,logLevel:Int32){
        
        //Create string to write or log
        let str = "\(convertLogLevelInString(log: logLevel))/\(tag) - \(text)"
        
        //Write log on console
        if consoleLevel >= logLevel && consoleLevel != DISABLED{
            NSLog(str, logLevel)
        }
        
        //Write log on file
        if !useOnlyLog && GLog.sharedInstance.fileHandle != nil && fileLevel >= logLevel && fileLevel != DISABLED {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
            formatter.timeZone = TimeZone.current
            let dateStr = formatter.string(from: Date())
            
            //write row on file
            vfprintf(GLog.sharedInstance.fileHandle, "\(dateStr) \(Bundle.main.bundleIdentifier!) \(str)\n", getVaList([]))
            
            //force write every time
            fflush(GLog.sharedInstance.fileHandle)
        }
    }
    
    //Convert string in log level (Int32 -> priority standard of NSLog)
    static func convertStringInLogLevel(text:String) -> Int32{
        switch text {
        case "LOG_EMERG":
            return LOG_EMERG
        case "LOG_ALERT":
            return LOG_ALERT
        case "LOG_CRIT":
            return LOG_CRIT
        case "LOG_ERR":
            return LOG_ERR
        case "LOG_WARNING":
            return LOG_WARNING
        case "LOG_NOTICE":
            return LOG_NOTICE
        case "LOG_INFO":
            return LOG_INFO
        case "LOG_DEBUG":
            return LOG_DEBUG
        default:
            return DISABLED
        }
    }
    
    //Convert Log level in string
    static func convertLogLevelInString(log:Int32) -> String{
        switch log {
        case LOG_EMERG:
            return "X"
        case LOG_ALERT:
            return "A"
        case LOG_CRIT:
            return "C"
        case LOG_ERR:
            return "E"
        case LOG_WARNING:
            return "W"
        case LOG_NOTICE:
            return "N"
        case LOG_INFO:
            return "I"
        case LOG_DEBUG:
            return "D"
        default:
            return "DISABLED"
        }
    }
    
    static func getDocumentDirectory() -> NSString{
        let docDirectory: NSString = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        return docDirectory
    }
    
    static func setDefaultTag() {
        GLog.tagDefault = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
    }
}
