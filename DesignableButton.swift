//
//  PauseButton.swift
//  RaiRadio
//
//  Created by bofh on 22/07/17.
//  Copyright © 2017 bofh. All rights reserved.
//

import UIKit
import GLKit

@IBDesignable
class DesignableUIButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.clipsToBounds = true
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.clipsToBounds = true
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable var rotation: Float = 0 {
        didSet {
            let radians = GLKMathDegreesToRadians(rotation)
            self.transform = self.transform.rotated(by: CGFloat(radians))
        }
    }
    
    @IBInspectable var shadowVisible : Bool = false {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 1.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOpacity : Float = 0.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOffsetX : CGFloat = 0 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOffsetY : CGFloat = 1.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var alwaysTemplate : Bool = false {
        didSet {
            self.setImage(self.image(for: .normal)?.withRenderingMode( (alwaysTemplate) ? .alwaysTemplate : .alwaysOriginal), for: .normal)
            self.imageView?.contentMode = .scaleAspectFit
        }
    }
    
    func setShadow(visible: Bool) {
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = false
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = (visible) ? UIColor.black.cgColor : UIColor.clear.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
    }
}
