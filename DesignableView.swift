//
//  DesignableView.swift
//  Fanteex
//
//  Created by Federico Gentile on 28/04/17.
//  Copyright © 2017 Fingerlinks. All rights reserved.
//

import UIKit
import GLKit

@IBDesignable
class DesignableUIView: UIView {
        
    @IBInspectable var rotation: Float = 0 {
        didSet {
            let radians = GLKMathDegreesToRadians(rotation)
            self.transform = self.transform.rotated(by: CGFloat(radians))
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            self.layer.borderWidth = 1
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.clipsToBounds = true
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderCornerRadius: CGFloat = 4 {
        didSet {
            self.layer.borderWidth = 1
            self.layer.cornerRadius = borderCornerRadius
        }
    }
    
    @IBInspectable var shadowVisible : Bool = false {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 1.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOpacity : Float = 0.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOffsetX : CGFloat = 0 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    @IBInspectable var shadowOffsetY : CGFloat = 1.5 {
        didSet {
            setShadow(visible: shadowVisible)
        }
    }
    
    func setShadow(visible: Bool) {
        guard visible else {
            return
        }
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = false
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = (visible) ? UIColor.black.cgColor : UIColor.clear.cgColor
        self.layer.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
    }
}

