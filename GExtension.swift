//
//  GExtension.swift
//  Fingerlinks
//
//  Created by Gilberto Santaniello on 02/12/16.
//  Copyright © 2016 Parcheggiatori. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import MapKit

//MARK: NSObject
public extension NSObject {
    func safeValue(forKey key: String) -> Any? {
        let copy = Mirror(reflecting: self)
        for child in copy.children.makeIterator() {
            if let label = child.label, label == key {
                return child.value
            }
        }
        return nil
    }
    
    func setSafe(dict: NSDictionary) {
        guard let keys = dict.allKeys as? [String] else {
            return
        }
        
        for key in keys {
            if let _ = self.safeValue(forKey: key) {
                if dict[key] is NSNull {
                    continue
                }
                self.setValue(dict[key], forKey: key)
            }
        }
    }
}

//MARK: Controller
public extension UIViewController{
    
    // don't use in viewDidLoad() !!!
    func GAlertSimple(typeAlert:UIAlertControllerStyle? = .alert , btnArrayName: [String], textTitle:String, textMessage:String, alert :@escaping (_ success: Int) -> ()){
        
        let alertController = UIAlertController(title: textTitle, message: textMessage, preferredStyle: typeAlert!)
        
        for i in 0..<btnArrayName.count{
            
            var style: UIAlertActionStyle = .default
            if btnArrayName.count > 0 && i == btnArrayName.count - 1{
                style = .cancel
            }
            
            alertController.addAction(UIAlertAction(title: btnArrayName[i], style: style, handler : { action in
                alert(i)
            }))
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // don't use in viewDidLoad() !!!
    func GPresentBASEViewController(identifier: String,animated:Bool = true) {
        let uiViewController = storyboard?.instantiateViewController(withIdentifier: identifier)
        self.present(uiViewController!, animated: animated, completion: nil)
    }
    
    // don't use in viewDidLoad() !!!
    func GPresentBASEWithOtherStoryboard(storyboard: String, identifier: String){
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: identifier)
        self.present(controller, animated: true, completion: nil)
    }
    
    // don't use in viewDidLoad() !!!
    func GPushBASEViewController(identifier: String) {
        let uiViewController = storyboard?.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(uiViewController!, animated: true)
    }
    
    
    func GHideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.GDismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func GDismissKeyboard() {
        view.endEditing(true)
    }
    
    //get class name
    class var className: String {
        return String(describing: self)
    }
    
    //simplify present
    func presentFromStoryboard(controller: UIViewController.Type, storyboardName: String? = nil) {
        let storyboard = (storyboardName == nil) ? self.storyboard : UIStoryboard(name: storyboardName!, bundle: nil)
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: controller.className) else {
            return
        }
        self.present(vc, animated: true, completion: nil)
        return
    }
    
}

//MARK: Component
public extension UITableView {
    func GScrollToBottom(animated: Bool, heightToAdd: CGFloat) {
        let SCREEN_HEIGHT = UIScreen.main.bounds.height
        
        var y: CGFloat = 0.0
        if self.contentSize.height > SCREEN_HEIGHT {
            y = self.contentSize.height - SCREEN_HEIGHT + heightToAdd
        }
        self.setContentOffset(CGPoint(x: 0, y: y), animated: animated)
    }
    
    func GSetOffsetToBottom(animated: Bool) {
        self.setContentOffset(CGPoint(x: 0, y: self.contentSize.height - self.frame.size.height), animated: true)
    }
    
    func GScrollToLastRow(animated: Bool) {
        if self.numberOfRows(inSection: 0) > 0 {
            self.scrollToRow(at: NSIndexPath(row: self.numberOfRows(inSection: 0) - 1, section: 0) as IndexPath, at: UITableViewScrollPosition.bottom, animated: false)
        }
    }
}

public extension String {
    func GFromBase64() -> String? {
        let option = Data.Base64DecodingOptions.ignoreUnknownCharacters
        guard let data = Data(base64Encoded: self, options: option) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func GToBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func GRandomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    var Ghtml2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    
    var Ghtml2String: String {
        return Ghtml2AttributedString?.string ?? ""
    }
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Strings", comment: "")
    }
}

//MARK: Graphics
public extension UIImageView {
    func GRoundImageView(imageView:UIImageView){
        imageView.layer.cornerRadius = imageView.frame.size.width/2
        imageView.clipsToBounds = true
    }
}

public extension UIColor{
    func GColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

//MARK: Notifications
@available(iOS 10.0, *)
public extension UNNotificationAttachment {
    public static func GCreate(identifier: String, image: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageFileIdentifier = identifier+".png"
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            guard let imageData = UIImagePNGRepresentation(image) else {
                return nil
            }
            try imageData.write(to: fileURL)
            let imageAttachment = try UNNotificationAttachment.init(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
        } catch {
            print("error " + error.localizedDescription)
        }
        return nil
    }
}

public extension UIView {
    
    func GLoadNib(named: String,owner: UIViewController) -> UIView {
        return Bundle.main.loadNibNamed(named, owner: owner, options: nil)![0] as! UIView
    }
    
    // use in viewDidLoad() !!!
    func GRounds(){
        self.layer.cornerRadius = 8
    }
    
    // use in viewDidLoad() !!!
    func GRounds(value: CGFloat){
        self.layer.cornerRadius = value
    }
    
    // round specific corners
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    // use in viewDidLoad() !!!
    func GShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 1
    }
    
    // use in viewDidLoad() !!!
    func GShadow(radius: CGFloat, offset: CGSize, opacity:Float){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
    }

    //get class name
    class var className: String {
        return String(describing: self)
    }
}

public extension NSDate {
    func GIsGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func GIsLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func GEqualToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func GAddDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func GAddHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}

