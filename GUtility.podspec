Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "GUtility"
s.summary = "GUtility is a repository of helpers files"
s.requires_arc = true

s.version = "0.5"
s.author = "Gilberto Santaniello"

s.homepage = "https://gitlab.materialapp.com/gibbo/GUtility"
s.source = {:git => "https://gitlab.materialapp.com/gibbo/GUtility.git", :tag => "#{s.version}"}

s.source_files = "*.{swift}"

end
