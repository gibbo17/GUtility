//
//  CheckBox.swift
//  Leghe
//
//  Created by Federico Gentile on 18/01/17.
//  Copyright © 2017 Quadronica S.R.L. All rights reserved.
//

import UIKit

@IBDesignable
class CheckBox: DesignableUIButton {
    // Images
    
    @IBInspectable var checkedImage: UIImage! = UIImage()
    @IBInspectable var uncheckedImage: UIImage! = UIImage()
    
    @IBInspectable var checkedBackgroundColor: UIColor = UIColor.clear
    @IBInspectable var uncheckedBackgroundColor: UIColor = UIColor.clear
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.backgroundColor = checkedBackgroundColor
                self.setImage(checkedImage, for: .normal)
            } else {
                self.backgroundColor = uncheckedBackgroundColor
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked(_:)), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }
    
    @objc func buttonClicked(_ sender: Any?) {
        isChecked = !isChecked
    }
}
