//
//  Utils.swift
//  Galactus
//
//  Created by Gilberto Santaniello on 10/10/17.
//  Copyright © 2017 Galactus. All rights reserved.
//

import Foundation
import MapKit

//MARK: request to get image data from url
public func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
    URLSession.shared.dataTask(with: url) {
        (data, response, error) in
        completion(data, response, error)
        }.resume()
}

//MARK: Shared Manager
public func sharedManager(objectToShare:Any, vc:UIViewController){
    let activityVC = UIActivityViewController(activityItems: [objectToShare], applicationActivities: nil)

    if UIDevice.current.userInterfaceIdiom == .pad {
        activityVC.popoverPresentationController?.sourceView = vc.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
        activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.init(rawValue: 0)
    }
    
    vc.present(activityVC, animated: true, completion: nil)

}

//MARK: Maps
public func createMapImage(lat:Double, long:Double, width: Int, height: Int) -> MKMapSnapshotter {
    let mapSnapshotOptions = MKMapSnapshotOptions()
    
    // Set the region of the map that is rendered.
    let location = CLLocationCoordinate2DMake(lat, long)
    let region = MKCoordinateRegionMakeWithDistance(location, 500, 500)
    mapSnapshotOptions.region = region
    
    // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
    mapSnapshotOptions.scale = UIScreen.main.scale
    
    // Set the size of the image output.
    mapSnapshotOptions.size = CGSize(width: width, height: height)
    
    // Show buildings and Points of Interest on the snapshot
    mapSnapshotOptions.showsBuildings = true
    mapSnapshotOptions.showsPointsOfInterest = true
    
    let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
    return snapShotter
}
